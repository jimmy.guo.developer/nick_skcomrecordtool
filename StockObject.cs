﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKCOM_Record_Tool
{
    public class StockObject
    {
        /// <summary>
        /// 股票編號
        /// </summary>
        public string Stock_Num { get; set; }
        /// <summary>
        /// 股票編號
        /// </summary>
        public string Stock_Name { get; set; }

        /*策略用參數*/
        /// <summary>
        /// 開盤排名
        /// </summary>
        public int Rank { get; set; }

        /// <summary>
        /// 黑名單狀態，0:否、1:是
        /// </summary>
        public int Blacklist { get; set; }

        /// <summary>
        /// 尚未結清，0:否、1:是
        /// </summary>
        public int onHand { get; set; } = default;

        /// <summary>
        /// 第一次收到tick時更新資料
        /// </summary>
        public bool GetOnceInfo { get; set; }

        /*基本資訊*/
        /// <summary>
        /// 最新一筆成交價
        /// </summary>
        public decimal nClose { get; set; }
        /// <summary>
        /// 最新一筆bid價
        /// </summary>
        public decimal nBid { get; set; }
        /// <summary>
        /// 最新一筆Ask價
        /// </summary>
        public decimal nAsk { get; set; }
        /// <summary>
        /// 最新一筆Tick時間
        /// </summary>
        public string Tick_Time { get; set; }

        public DateTime Last_Trade_Time { get; set; }

        /// <summary>
        /// 昨價
        /// </summary>
        public decimal nRef { get; set; }

        /// <summary>
        /// 昨輛
        /// </summary>
        public int nYQty { get; set; }

        /// <summary>
        /// 今總量
        /// </summary>
        public int nTQty { get; set; }

        /// <summary>
        /// 開盤價
        /// </summary>
        public decimal nOpen { get; set; } = default;

        /// <summary>
        /// 今日高
        /// </summary>
        public decimal nHigh { get; set; } = default;
        /// <summary>
        /// 日高發生時間
        /// </summary>
        public DateTime nHigh_Time { get; set; }

        /// <summary>
        /// 今日低
        /// </summary>
        public decimal nLow { get; set; } = default;

        /// <summary>
        /// 日低發生時間
        /// </summary>
        public DateTime nLow_Time { get; set; }

        /// <summary>
        /// 昨日高
        /// </summary>
        public decimal nYHigh { get; set; }
        /// <summary>
        /// 昨日低
        /// </summary>
        public decimal nYLow { get; set; }


        /*五檔資訊*/
        public decimal nBestBid1 { get; set; }
        public int nBestBidQty1 { get; set; }
        public decimal nBestBid2 { get; set; }
        public int nBestBidQty2 { get; set; }
        public decimal nBestBid3 { get; set; }
        public int nBestBidQty3 { get; set; }
        public decimal nBestBid4 { get; set; }
        public int nBestBidQty4 { get; set; }
        public decimal nBestBid5 { get; set; }
        public int nBestBidQty5 { get; set; }
        public decimal nBestAsk1 { get; set; }
        public int nBestAskQty1 { get; set; }
        public decimal nBestAsk2 { get; set; }
        public int nBestAskQty2 { get; set; }
        public decimal nBestAsk3 { get; set; }
        public int nBestAskQty3 { get; set; }
        public decimal nBestAsk4 { get; set; }
        public int nBestAskQty4 { get; set; }
        public decimal nBestAsk5 { get; set; }
        public int nBestAskQty5 { get; set; }

        public void nClose_to_all_day(decimal nClose, DateTime time)
        {
            //預設值
            if (this.nHigh == 0) this.nHigh = 0;
            if (this.nLow == 0) this.nLow = 9999999;
            if (nClose > this.nHigh) { this.nHigh = nClose; this.nHigh_Time = time; }
            if (nClose < this.nLow) { this.nLow = nClose; this.nLow_Time = time; }
        }

        public void Count_nTQty(int nQty)
        {
            this.nTQty += nQty;
        }
    }
}
