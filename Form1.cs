﻿using SKCOMLib;
using Spire.Xls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;
using System.Xml;

namespace SKCOM_Record_Tool
{
    public partial class Form1 : Form
    {
        #region SKCOM Variable
        //----------------------------------------------------------------------
        // SKCOM Variable
        //----------------------------------------------------------------------
        SKCenterLib m_pSKCenter;
        SKReplyLib m_pSKReply;
        SKOrderLib m_pSKOrder;
        SKQuoteLib m_SKQuoteLib = null;
        SKCOMLib.SKReplyLib m_SKReplyLib = null;
        int m_nCode;
        string FULL_ACCOUNT;
        public SKQuoteLib SKQuoteLib
        {
            get { return m_SKQuoteLib; }
            set { m_SKQuoteLib = value; }
        }
        public SKOrderLib OrderObj
        {
            get { return m_pSKOrder; }
            set { m_pSKOrder = value; }
        }
        public SKReplyLib SKReplyLib
        {
            get { return m_SKReplyLib; }
            set { m_SKReplyLib = value; }
        }
        #endregion



        /// <summary>
        /// SQL資料存取物件
        /// </summary>
        SQLObject SQL;
        /// <summary>
        /// 工具物件
        /// </summary>
        ToolObject TOOL;
        /// <summary>
        /// 計時器-非同步任務呼叫
        /// </summary>
        System.Timers.Timer Async_Task_Timer = new System.Timers.Timer();
        /// <summary>
        /// 非同步任務回呼
        /// </summary>
        /// <param name="sMessage"></param>
        private delegate void DelShowMessage(string sMessage);
        /// <summary>
        /// 啟動程式時傳入的參數
        /// </summary>
        string[] args = Environment.GetCommandLineArgs();
        /// <summary>
        /// LOG儲存路徑
        /// </summary>
        string LOGPATH;
        /// <summary>
        /// 累計收到TICK數量
        /// </summary>
        int TICKCOUNT = 0;
        bool CHECK_TICK_TABLE = false;
        bool REQUEST_STOCK_INFO = false;
        DateTime LAST_TICK_RECEIVE;
        StockObject[] STOCKS = new StockObject[40000];
        bool DOWNLOAD_DATA = false;
        string TODAY_STR;


        public Form1()
        {
            InitializeComponent();
            InitializeFormValue();
            System_Setting_Value_Load();
            InitializeGlobalVariable();

        }

        public void Form1_Shown(object sender, EventArgs e)
        {
            if (args.Contains("-AutoQuit"))
            {
                Execute_Async_Task_Timer();//執行計時器任務
                Excute_Trade_Task();
            }

        }

        /// <summary>
        /// 初始化winform的內容值
        /// </summary>
        private void InitializeFormValue()
        {
            //SQL_Server.Text = Get_Config_Value("SQL_SERVER");
            SQL_DB.Text = Get_Config_Value("SQL_DB");
            SQL_UID.Text = Get_Config_Value("SQL_UID");
            SQL_PWD.Text = Get_Config_Value("SQL_PWD");
            API_Account.Text = Get_Config_Value("API_ACCOUNT");
            API_Password.Text = Get_Config_Value("API_PASSWORD");
            Log_Path.Text = Get_Config_Value("LOGPATH");
            SelectDate.Value = DateTime.Now;
        }

        /// <summary>
        /// 初始化環境變數
        /// </summary>
        private void InitializeGlobalVariable()
        {
            string CURRENTDIRECTORY = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            SQL = new SQLObject();
            SQL.SetConnectingValue(SQL_Server.Text, SQL_DB.Text, SQL_UID.Text, SQL_PWD.Text, "10");
            TOOL = new ToolObject();
            if (Get_Config_Value("LOGPATH") == "")
            {
                LOGPATH = CURRENTDIRECTORY;
                TOOL.Root_Folder_Path = CURRENTDIRECTORY;
            }
            else
            {
                LOGPATH = Get_Config_Value("LOGPATH");
                TOOL.Root_Folder_Path = LOGPATH;
            }
            Check_Folder_Exist();
            TODAY_STR = DateTime.Now.ToString("yyyy-MM-dd");
            m_pSKCenter = new SKCenterLib();
            m_pSKReply = new SKReplyLib();
            m_pSKOrder = new SKOrderLib();
            m_SKQuoteLib = new SKQuoteLib();
            m_pSKReply.OnReplyMessage += new _ISKReplyLibEvents_OnReplyMessageEventHandler(this.OnAnnouncement);
            m_SKQuoteLib.OnConnection += new _ISKQuoteLibEvents_OnConnectionEventHandler(m_SKQuoteLib_OnConnection);
            for (int i = 0; i < 40000; i++)
            {
                STOCKS[i] = new StockObject();
            }
            LAST_TICK_RECEIVE = DateTime.Parse("2100-01-01 00:00:00");
        }

        /// <summary>
		/// 從檔案中載入系統參數
		/// </summary>
		public void System_Setting_Value_Load()
        {
            string LOGPATH = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            using (XmlReader reader = XmlReader.Create(LOGPATH + @"\System_Config.xml"))
            {
                while (reader.Read())
                {
                    if (reader.IsStartElement())
                    {
                        switch (reader.Name.ToString())
                        {
                            case "Server":
                                SQL_Server.Text = reader.ReadString();
                                break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 檢查系統專用資料夾是否存在
        /// </summary>
        private void Check_Folder_Exist()
        {
            if (Log_Path.Text != "")
            {
                TOOL.Root_Folder_Path = Log_Path.Text;
                TOOL.Check_Folder_Exist(@"/output");
                TOOL.Check_Folder_Exist(@"/output/Excel");
                TOOL.Check_Folder_Exist(@"/output/log");
            }
        }

        public void Excute_Trade_Task()
        {
            try
            {
                SKCOM_Server_Login();       //登入群益系
                SKCOM_OrderLib_Initialize();//交易帳戶初始化
                SKCOM_ReadCertByID();       //讀取憑證
                SKCOM_GetUserAccount();     //取得交易帳號
                SKCOM_Reply_Connect();      //委託回報
                SKCOM_Quote_Login();        //登入報價主機
                if (!args.Contains("-AutoQuit"))
                {
                    Execute_Async_Task_Timer();//執行計時器任務
                }
            }
            catch (Exception ex)
            {
                SQL.Add_SQL_Log("Excute_Trade_Task", ex.Message);
            }
        }

        /// <summary>
        /// 啟動計時器任務
        /// </summary>
        private void Execute_Async_Task_Timer()
        {
            Async_Task_Timer.Enabled = true;
            Async_Task_Timer.Interval = 5000;//間隔5秒
            Async_Task_Timer.Start();
            Async_Task_Timer.Elapsed += new System.Timers.ElapsedEventHandler(Async_Task_Manager);
        }

        public void Async_Task_Manager(object sender, EventArgs e)
        {

            string gtm = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fffff");
            string msg = "WriteMessage@" + gtm;
            if (DateTime.Now > LAST_TICK_RECEIVE.AddMinutes(20) && DOWNLOAD_DATA == false)
            {
                DOWNLOAD_DATA = true;
                AsyncTaskDeal("Download_Tick_Data");

            }
        }

        /// <summary>
        /// 跨執行續事件處理
        /// </summary>
        /// <param name="sMessage">執行內容</param>
        public void AsyncTaskDeal(string sMessage)
        {
            if (this.InvokeRequired) // 若非同執行緒
            {
                DelShowMessage del = new DelShowMessage(AsyncTaskDeal); //利用委派執行
                this.Invoke(del, sMessage);
            }
            else // 同執行緒
            {
                string[] words = sMessage.Split('@');
                string op = words[0];
                switch (op)
                {
                    case "Add_Err_Msg":
                        TOOL.Add_Log(words[1], words[2]);
                        break;
                    case "WriteMessage":
                        WriteMessage(words[1]);
                        break;
                    case "Clean_Trade":
                        //Clean_All_Undone_Trade();
                        break;
                    case "Excute_Trade_Task":
                        Excute_Trade_Task();
                        break;
                    case "Download_Tick_Data":
                        Download_Tick_Data("");
                        Download_Daily_Data("");
                        Download_Best5_Data("");
                        Delete_Tick_Table();
                        break;
                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// 新增listbox訊息
        /// </summary>
        /// <param name="strMsg">訊息內容</param>
        private void WriteMessage(string strMsg)
        {
            string Message_Time = DateTime.Now.ToString("【yyyy-MM-dd HH:mm:ss.fffffff】 ");
            listInformation.Items.Add(Message_Time + strMsg);
            listInformation.SelectedIndex = listInformation.Items.Count - 1;
            Graphics g = listInformation.CreateGraphics();
            int hzSize = (int)g.MeasureString(listInformation.Items[listInformation.Items.Count - 1].ToString(), listInformation.Font).Width;
            listInformation.HorizontalExtent = hzSize;
            TOOL.Add_Log("system_log", strMsg);
            Application.DoEvents();
        }

        /// <summary>
        /// 新增listbox訊息
        /// </summary>
        /// <param name="nCode">API回傳的狀態碼</param>
        private void WriteMessage(int nCode)
        {
            string Message_Time = DateTime.Now.ToString("【yyyy-MM-dd HH:mm:ss.fffffff】 ");
            string CodeMsg = m_pSKCenter.SKCenterLib_GetReturnCodeMessage(nCode);
            listInformation.Items.Add(Message_Time + CodeMsg);
            listInformation.SelectedIndex = listInformation.Items.Count - 1;
            Graphics g = listInformation.CreateGraphics();
            int hzSize = (int)g.MeasureString(listInformation.Items[listInformation.Items.Count - 1].ToString(), listInformation.Font).Width;
            listInformation.HorizontalExtent = hzSize;
            TOOL.Add_Log("system_log", CodeMsg);
            Application.DoEvents();
        }

        /// <summary>
        /// 下載當日TICK資料
        /// </summary>
        private void Download_Tick_Data(string selectedDate)
        {
            DataTable ds = SQL.Search("SELECT * FROM [dbo].[Stock] WHERE Stock_BlackList_R != '手動建立' ORDER BY CONVERT(int,Stock_Num) ASC");
            string table_name = SQL.GetResult("SELECT TOP(1) TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE 'tick_%' ORDER BY TABLE_NAME DESC");
            string select_date = table_name.Replace("tick_", "");
            string shortDate = selectedDate.Replace("-", "");
            if (selectedDate!= "")
            {
                table_name = "tick_" + shortDate;
                select_date = selectedDate.Replace("-", "");
            }
            foreach (DataRow row in ds.Rows)
            {
                
                string Stock_Num = row["Stock_Num"].ToString();
                string Stock_Name = row["Stock_Name"].ToString();
                WriteMessage(shortDate + " - [" + Stock_Num + "]tick下載開始");
                string query = @"SELECT '" + select_date + @"' as 日期
					        ,format(tick_time, 'HH:mm:ss.fff') as tick時間
					        ,stock_num as 股票代號
					        ,'" + Stock_Name + @"' as 股票名稱
					        ,nBid as bid價
					        ,nAsk as ask價
					        ,nClose as 成交價
					        ,CASE WHEN nClose <= nBid AND nBid != 0 THEN nQty ELSE 0 END AS bid量
					        ,CASE WHEN nClose >= nAsk AND nAsk != 0 THEN nQty ELSE 0 END AS ask量
					        ,'0' AS 累計bid量
					        ,'0' AS 累計ask量
					        ,nPtr as 成交序號
                            ,B.daily_nTQty AS 前一天總量
                            ,B.daily_nClose AS 前一天收盤價
                            ,C.daily_nClose AS 當天收盤價
					        FROM dbo." + table_name + @" AS a
                            LEFT OUTER JOIN	    (SELECT TOP(1) *
					                             FROM [SKCOM].[dbo].[daily_data]
					                             WHERE [daily_data].daily_stock_num = '"+ Stock_Num + @"'
					                             AND CONVERT(varchar(100), daily_date, 112) < '"+ shortDate + @"'
					                             ORDER BY daily_date DESC) AS B ON (1=1)
                            LEFT OUTER JOIN	    (SELECT TOP(1) *
					                             FROM [SKCOM].[dbo].[daily_data]
					                             WHERE [daily_data].daily_stock_num = '" + Stock_Num + @"'
					                             AND CONVERT(varchar(100), daily_date, 112) = '" + shortDate + @"'
					                             ORDER BY daily_date DESC) AS C ON (1=1)
					        WHERE stock_num = '" + Stock_Num + @"' 

					        ORDER BY Convert(int,nPtr) ASC";
                DataTable tick_ds = SQL.Search(query);
                if (tick_ds.Rows.Count > 0)
                {
                    int Count_Bid = 0;
                    int Count_Ask = 0;
                    foreach (DataRow tick_row in tick_ds.Rows)
                    {
                        Count_Bid += Convert.ToInt32(tick_row["bid量"].ToString());
                        Count_Ask += Convert.ToInt32(tick_row["ask量"].ToString());
                        tick_row["累計bid量"] = Count_Bid.ToString();
                        tick_row["累計ask量"] = Count_Ask.ToString();
                    }
                    TOOL.Datatable_Insert_Xls(tick_ds, @"/" + table_name.Replace("tick_", "") + @"/tick/" + Stock_Num + "_" + table_name.Replace("tick_", "") + "-tick", "none", false);
                }
                WriteMessage(shortDate +" - ["+ Stock_Num + "]tick下載完成");
            }

        }

        private void Delete_Tick_Table()
        {
            string query = @"SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE 'tick_%' ORDER BY TABLE_NAME DESC";
            DataTable ds = SQL.Search(query);
            int count = 0;
            foreach (DataRow row in ds.Rows)
            {
                string table_name = row["TABLE_NAME"].ToString();
                if (count > 5)
                {
                    query = @"DROP TABLE " + table_name;
                    SQL.Execute(query);
                }
                count++;
            }

        }

        private void Download_Best5_Data(string selectedDate)
        {
            DataTable ds = SQL.Search("SELECT * FROM [dbo].[Stock] WHERE Stock_BlackList_R != '手動建立' ORDER BY CONVERT(int,Stock_Num) ASC");
            string table_name = SQL.GetResult("SELECT TOP(1) TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE 'tick_%' ORDER BY TABLE_NAME DESC");
            string select_date = table_name.Replace("tick_", "");
            if (selectedDate != "")
            {
                table_name = "tick_" + selectedDate.Replace("-", "");
                select_date = selectedDate.Replace("-", "");
            }
            foreach (DataRow row in ds.Rows)
            {
                string Stock_Num = row["Stock_Num"].ToString();
                string Stock_Name = row["Stock_Name"].ToString();
                string query = @"SELECT format(receice_datetime, 'HH:mm:ss.fff') as 收到時間
						        ,CAST(nBestBid1 as float) / CAST(100 as float) as bid_1
						        ,nBestBidQty1 as bid_1量
						        ,CAST(nBestBid2 as float) / CAST(100 as float) as bid_2
						        ,nBestBidQty2 as bid_2量
						        ,CAST(nBestBid3 as float) / CAST(100 as float) as bid_3
						        ,nBestBidQty3 as bid_3量
						        ,CAST(nBestBid4 as float) / CAST(100 as float) as bid_4
						        ,nBestBidQty4 as bid_4量
						        ,CAST(nBestBid5 as float) / CAST(100 as float) as bid_5
						        ,nBestBidQty5 as bid_5量
						        ,CAST(nBestAsk1 as float) / CAST(100 as float) as ask_1
						        ,nBestAskQty1 as ask_1量
						        ,CAST(nBestAsk2 as float) / CAST(100 as float) as ask_2
						        ,nBestAskQty2 as ask_2量
						        ,CAST(nBestAsk3 as float) / CAST(100 as float) as ask_3
						        ,nBestAskQty3 as ask_3量
						        ,CAST(nBestAsk4 as float) / CAST(100 as float) as ask_4
						        ,nBestAskQty4 as ask_4量
						        ,CAST(nBestAsk5 as float) / CAST(100 as float) as ask_5
						        ,nBestAskQty5 as ask_5量
						        FROM [dbo].best5
						        WHERE stock_num = '" + Stock_Num + @"' 
						        ORDER BY receice_datetime ASC";
                DataTable best5_ds = SQL.Search(query);
                TOOL.Datatable_Insert_Xls(best5_ds, @"/" + table_name.Replace("tick_", "") + @"/best5/" + Stock_Num + "_" + table_name.Replace("tick_", "") + "-best5", "none", false);
            }

        }
        /// <summary>
        /// 下載當日股票資訊
        /// </summary>
        private void Download_Daily_Data(string selectedDate)
        {
            try
            {
                //註冊授權
                Spire.License.LicenseProvider.SetLicenseKey("JKFIWPyWXbLKNOIBAHIMabhlPd00tFIF6rW6dFGnBCBVl9draunSqL5fVB2PhDQ2m4T6dJnlOzNqD81qYZ92gUhIMW0Z5cIm/HTIco176BqrYINLwGWxRmP76itKB5IISkueibbF3YTZOgVcg+tGD/wvb0KqzVQ4BZVMJqP01qHrV3q5sM3u7iITJsf9xWmFPYkbZLlblhpmDklrCJbBSc95bNSsg+pyXxF1kB8WiL1Xeptw8Fo2y0xnfjV/b0nng5g47nDn/XhaPyJvHFxRD19RsecZnB3gFl+XVwOjhvO18O58dZjwIYg4WUQLprg6gDo7mF8n/kdbpPBjIP7eXSMNqljJXXGu5rK0sfAfOAj4CBM8e7JaaB9dfrgqCWelAhcM+B5RzQPxg+PxLHsqJp2fi6+ly17V2ffBo6QQyjQFUoAKlfIiSDq0DNdsQdLW+pQyPRhp8esFNhEZOGoBZGbdpT/5IslXhg5TaI1o0svzY77Cd7zgvaD7+dpYfovVhGp6Jv8Et4C94W/B59lWPIbh9rs3wrRCsZpBoPMn/39vYLHsGY02ymxEwB1fGEdURSR7+8Jvntj5lfwafqUWgM9UwH4BJU5TADBMiohrKXxZV3O2XjdCa1gzpwTdlZo26TcS0/pdtHDZ95hcracB4AEIX/HoFSSIR7bUtccFjUW7uLWEBh2kpf425CLOkSEnOslAgqPwep6gxc4hWlZxWxn6A55AbcHDtA70sJkObbLPCZFr2uGMpw4vBUBLEEDLZLaPs3kuOGWuf9e8vsShN0AEiP0PDlkpFmUgU6tdMHEG35evCIS4/l0bI7IFoBiqj2h2H59b7ZNYvl+FZdxgMTxXFA/eEazg4Vj3ZW4wBUA371PJgiD9inb7LQJcMCdyJTf2JO71KZPDYAeGkGr2CrBbBBqjUzp55SDVtIch+1Vmnvl9nDmFXFncKOfn11pbStA872Wqut0chojxhvXWXUsuHxeNIBvmKJy2nXl8zFlNLIG+f/jlt10fI4VD83kGGlTam/YOvrkhc7XnPRDpNRNsz2SYNYIJTp1kHUGNJLmKhMIEFHy+wEuG7AawK3E35HSFvT+brILdg4oa5iKx+PHZ3mdRYMcrcv8i0UYMibtlG2tYHevtQCN55OxtleYYbOQtJuASe7jD/44vb5fPr4zLMx0gJWDDb9DEy1LjBdfkzibaMr4/NQHwgcrzHcrHXL7oRffj5bmopDE4FFVWc5jw+znmZUoxKJaJx4CInqtrefdXACS7acBjj5RfWztNxUKFhZAocXylZ0/fzb/vo/JZl2oEnx6m+gTWRRsmHaXubnJVhuYfY4DLPFh6GCsUxje/EfsSoAmNWov2tAaZlUV1Q3BpQarQI5JB3z7JVZknIvA8rDP2JrZug9waPmvYXw35eddg+20KzHFv9/obGwx0SPV4fX5BLDk0sG3j/ZuChfqja5SicqdTrj8NDfHONRxJN3gqTxWirH3Vm3FaqA==");
                //宣告spire 物件
                if (selectedDate != "")
                {
                    TODAY_STR = selectedDate;
                }
                //TODAY_STR = "2022-01-21";
                Workbook book = new Workbook();
                string sqlquery = @"SELECT  [daily_date]		AS '日期'
		                                ,[daily_stock_num]	AS '股票代號'
		                                ,[daily_stock_name]	AS '股票名稱'
		                                ,[daily_nTQty]		AS '總量'
		                                ,[daily_nRef]		AS '昨價'
		                                ,[daily_nHigh]		AS '日高'
		                                ,[daily_nLow]		AS '日低'
		                                ,[daily_nOpen]		AS '開盤'
		                                ,[daily_nClose]		AS '收盤'
		                                ,[daily_raise]		AS '震幅'
                                FROM [SKCOM].[dbo].[daily_data]
                                WHERE CONVERT(varchar,daily_date,23) = '" + TODAY_STR + @"'
                                ORDER BY [daily_stock_num] ASC";
                DataTable ds = SQL.Search(sqlquery);
                int index = 0;
                string xlsname = TODAY_STR + "個股資訊下載";
                if (ds.Rows.Count > 2)
                {
                    Worksheet sheet = book.Worksheets.Add(TODAY_STR);
                    sheet.InsertDataTable(ds, true, 1, 1);
                    sheet.AllocatedRange.AutoFitColumns();
                    index++;
                }
                book.Worksheets[0].Visibility = WorksheetVisibility.Hidden;
                book.Worksheets[1].Visibility = WorksheetVisibility.Hidden;
                book.Worksheets[2].Visibility = WorksheetVisibility.Hidden;
                book.SaveToFile(Log_Path.Text + @"\output\Relesae\" + xlsname + ".xlsx");
                TOOL.Open_File_Folder(Log_Path.Text + @"\output\Relesae\");
            }
            catch (Exception ex)
            {
                WriteMessage("下載今日個股資訊失敗，原因為：" + ex.Message);
            }

        }

        /*** App.config 存取相關 Start ***/

        /// <summary>
        /// 儲存App.config的變數值
        /// </summary>
        /// <param name="key">變數名稱</param>
        /// <param name="value">變數值</param>
        private void Save_Config_Value(string key, string value)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.AppSettings.Settings[key].Value = value;
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
        }

        /// <summary>
        /// 讀取App.config的值
        /// </summary>
        /// <param name="key">變數名稱</param>
        /// <returns>變數值</returns>
        private string Get_Config_Value(string key)
        {

            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            if (config.AppSettings.Settings[key] is null)
            {
                return "";
            }
            return config.AppSettings.Settings[key].Value.ToString();
        }

        /*** App.config 存取相關 End ***/

        /// <summary>
        /// 登入群益主機
        /// </summary>
        public void SKCOM_Server_Login()
        {
            int m_nCode;
            Server_State.Text = "登入中...";
            Application.DoEvents();
            //指定API log儲存位置
            m_pSKCenter.SKCenterLib_SetLogPath(Log_Path.Text + "/output/log/CapitalLog/");
            //登入群益主機
            m_nCode = m_pSKCenter.SKCenterLib_Login(API_Account.Text.Trim(), API_Password.Text.Trim());
            if (m_nCode == 0)
            {
                Server_State.Text = "登入成功";
                WriteMessage("登入群益主機成功");
            }
            else
            {
                Server_State.Text = "登入失敗";
                WriteMessage("登入群益主機失敗 錯誤代碼：" + m_nCode + "錯誤原因如下：");
                WriteMessage(m_nCode);
            }
            Application.DoEvents();
        }

        /// <summary>
        /// 交易帳戶初始化
        /// </summary>
        public void SKCOM_OrderLib_Initialize()
        {
            WriteMessage("交易帳戶：初始化開始");
            m_nCode = m_pSKOrder.SKOrderLib_Initialize();
            if (m_nCode == 0)
            {
                m_pSKOrder.OnAccount += new _ISKOrderLibEvents_OnAccountEventHandler(m_OrderObj_OnAccount);
                m_pSKOrder.OnRealBalanceReport += new _ISKOrderLibEvents_OnRealBalanceReportEventHandler(m_pSKOrder_OnRealBalanceReport);
                WriteMessage("交易帳戶：初始化成功");
            }
            else
            {
                WriteMessage("交易帳戶：初始化失敗 錯誤代碼：" + m_nCode + "錯誤原因如下：");
                WriteMessage(m_nCode);
            }
        }

        /// <summary>
        /// 讀取憑證
        /// </summary>
        public void SKCOM_ReadCertByID()
        {
            m_nCode = m_pSKOrder.ReadCertByID(API_Account.Text);
            if (m_nCode == 0)
            {
                WriteMessage("讀取憑證：讀取成功");
            }
            else
            {
                WriteMessage("讀取憑證：讀取化失敗 錯誤代碼：" + m_nCode + "錯誤原因如下：");
                WriteMessage(m_nCode);
            }
        }

        /// <summary>
        /// 取得交易帳號
        /// </summary>
        public void SKCOM_GetUserAccount()
        {
            m_nCode = m_pSKOrder.GetUserAccount();
            if (m_nCode == 0)
            {
                WriteMessage("取得交易帳號：取得成功");
            }
            else
            {
                WriteMessage("取得交易帳號：取得失敗 錯誤代碼：" + m_nCode + "錯誤原因如下：");
                WriteMessage(m_nCode);
            }
        }

        /// <summary>
        /// 委託回報連線
        /// </summary>
        public void SKCOM_Reply_Connect()
        {
            m_SKReplyLib = new SKReplyLib();
            Reply_State.Text = "連線中...";
            Application.DoEvents();
            m_nCode = m_SKReplyLib.SKReplyLib_ConnectByID(API_Account.Text.Trim());
            if (m_nCode == 0)
            {
                Reply_State.Text = "連線成功";
                WriteMessage("委託回報主機成功");
                m_SKReplyLib.OnData += new _ISKReplyLibEvents_OnDataEventHandler(this.OnData);
            }
            else
            {
                Reply_State.Text = "連線失敗";
                WriteMessage("委託回報主機失敗 錯誤代碼：" + m_nCode + "錯誤原因如下：");
                WriteMessage(m_nCode);
            }
            Application.DoEvents();
        }

        /// <summary>
        /// 登入報價主機
        /// </summary>
        public void SKCOM_Quote_Login()
        {
            Quote_State.Text = "登入中...";
            Application.DoEvents();
            m_nCode = m_SKQuoteLib.SKQuoteLib_EnterMonitorLONG();
            if (m_nCode == 0)
            {
                Quote_State.Text = "登入成功";
                WriteMessage("登入報價主機成功");
                m_SKQuoteLib.OnNotifyServerTime += new _ISKQuoteLibEvents_OnNotifyServerTimeEventHandler(m_SKQuoteLib_OnNotifyServerTime);
                m_SKQuoteLib.OnNotifyTicksLONG += new _ISKQuoteLibEvents_OnNotifyTicksLONGEventHandler(m_SKQuoteLib_OnNotifyTicks);
                m_SKQuoteLib.OnNotifyHistoryTicksLONG += new _ISKQuoteLibEvents_OnNotifyHistoryTicksLONGEventHandler(m_SKQuoteLib_OnNotifyHistoryTicks);
            }
            else
            {
                Quote_State.Text = "登入失敗";
                WriteMessage("登入報價主機失敗 錯誤代碼：" + m_nCode + "錯誤原因如下：");
                WriteMessage(m_nCode);
            }
            Application.DoEvents();
        }

        /// <summary>
        /// 呼叫群益主機，連線後如逾15分鐘未再次呼叫，則群益主機會中斷連線
        /// </summary>
        public void Call_SKCOM_Regular(object source, ElapsedEventArgs e)
        {
            try
            {
                m_SKQuoteLib.OnNotifyServerTime += new _ISKQuoteLibEvents_OnNotifyServerTimeEventHandler(m_SKQuoteLib_OnNotifyServerTime);
                Application.DoEvents();
            }
            catch
            {
                string gtm = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fffff");
                SQL.Add_SQL_Log("call_server_error", "呼叫主機失敗");
            }
        }

        /// <summary>
        /// 訂閱商品報價
        /// </summary>
        public void SKQuoteLib_RequestStocks()
        {
            DataTable ds = new DataTable();
            string query = @"SELECT TOP (" + Request_Max_Num.Text + @") *
							FROM [dbo].[Stock]
							WHERE Stock_Account = '" + API_Account.Text + @"'
							ORDER BY Stock_No ASC";
            ds = SQL.Search(query);
            if (ds.Rows == null || ds.Rows.Count < 1)
            {
                WriteMessage("目前下單帳號無紀錄任何股票清單，請重新檢查執行");
            }
            else
            {
                string stock_list = "";
                short sPage = 0;
                foreach (DataRow row in ds.Rows)
                {
                    string stock_num = row["Stock_Num"].ToString();
                    stock_list += stock_num + ",";
                }
                if (stock_list.Length >= 3)
                {
                    stock_list = stock_list.Remove(stock_list.LastIndexOf(","), 1);
                }
                m_nCode = m_SKQuoteLib.SKQuoteLib_RequestStocks(ref sPage, stock_list.Trim());
                if (m_nCode == 0)
                {
                    WriteMessage("已成功訂閱所有商品報價");
                }
                else
                {
                    WriteMessage("訂閱報價失敗，原因如下：");
                    WriteMessage(m_nCode);
                }
            }
        }

        /// <summary>
        /// 訂閱tick及五檔
        /// </summary>
        public void SKCOM_RequestTicks_and_Best5()
        {
            DataTable ds = new DataTable();
            string query = @"SELECT TOP (" + Request_Max_Num.Text + @") *
							FROM [dbo].[Stock]
							WHERE Stock_Account = '" + API_Account.Text + @"'
							ORDER BY Stock_No ASC";
            ds = SQL.Search(query);
            if (ds.Rows == null || ds.Rows.Count < 1)
            {
                WriteMessage("目前下單帳號無紀錄任何股票清單，請重新檢查執行");
            }
            else
            {
                short sPage = 0;
                foreach (DataRow row in ds.Rows)
                {
                    string stock_num = row["Stock_Num"].ToString();
                    int m_nCode = m_SKQuoteLib.SKQuoteLib_RequestTicks(ref sPage, stock_num.Trim());
                    if (m_nCode != 0)
                    {
                        WriteMessage("股票編號：" + stock_num + " 訂閱Ticks及五檔失敗 錯誤代碼為：" + m_nCode.ToString() + " page變數為：" + sPage.ToString());
                    }
                    else
                    {
                        WriteMessage("股票編號：" + stock_num + " 訂閱成功 page變數為：" + sPage.ToString());

                    }
                    sPage++;
                }
            }
        }

        private void Update_Stock_Info()
        {
            try
            {
                WriteMessage("開始更新股票資訊");
                if (!REQUEST_STOCK_INFO)
                {
                    Thread.Sleep(2000);
                    REQUEST_STOCK_INFO = true;
                }
                DateTime TODAY = DateTime.Now;
                string datetime = DateTime.Now.ToString("yyyy-MM-dd");
                if (TODAY.DayOfWeek == DayOfWeek.Saturday)
                {
                    datetime = TODAY.AddDays(-1).ToString("yyyy-MM-dd");
                }
                else if (TODAY.DayOfWeek == DayOfWeek.Sunday)
                {
                    datetime = TODAY.AddDays(-2).ToString("yyyy-MM-dd");

                }
                else if (TODAY.DayOfWeek == DayOfWeek.Monday && DateTime.Now < DateTime.Parse(datetime + " 08:00:00"))
                {
                    datetime = TODAY.AddDays(-3).ToString("yyyy-MM-dd");
                }
                TODAY_STR = datetime;
                DataTable ds = new DataTable();
                string query = @"SELECT TOP (" + Request_Max_Num.Text + @") *
							FROM [dbo].[Stock]
							LEFT OUTER JOIN dbo.daily_data AS D1 
							ON (D1.daily_stock_num = Stock_Num AND daily_date = (SELECT TOP(1) D2.daily_date FROM dbo.daily_data AS D2 WHERE D2.daily_stock_num = D1.daily_stock_num AND D2.daily_date < '" + datetime + @"' ORDER BY D2.daily_date DESC))
							WHERE Stock_Account = '" + API_Account.Text + @"'
							ORDER BY Stock_No ASC";
                ds = SQL.Search(query);
                foreach (DataRow row in ds.Rows)
                {
                    string stock_num = row["Stock_Num"].ToString();
                    string stock_name = row["Stock_Name"].ToString();
                    string stock_blacklist = row["Stock_BlackList"].ToString();
                    SKSTOCKLONG pSKStock = new SKSTOCKLONG();
                    int nCode = m_SKQuoteLib.SKQuoteLib_GetStockByNoLONG(stock_num.Trim(), ref pSKStock);
                    if (nCode != 0)
                    {
                        WriteMessage("股票編號：" + stock_num + "更新資訊失敗，錯誤代碼為" + nCode.ToString());
                        string sqlquery = @"MERGE [dbo].[daily_data] AS T
								USING (VALUES('{#daily_date}','{#daily_stock_num}','{#daily_stock_name}','{#daily_nTQty}','{#daily_nRef}','{#daily_nHigh}','{#daily_nLow}','{#daily_nOpen}','{#daily_nClose}','{#daily_percent}','{#daily_sStockIdx}'))
								AS S ([daily_date], [daily_stock_num],[daily_stock_name],[daily_nTQty],[daily_nRef],[daily_nHigh],[daily_nLow],[daily_nOpen],[daily_nClose],[daily_percent],[daily_sStockIdx])
								ON (S.[daily_date] = T.[daily_date] AND S.[daily_stock_num] = T.[daily_stock_num] )
								WHEN MATCHED THEN 
								UPDATE SET [daily_stock_name] = S.[daily_stock_name]
								,[daily_nTQty] = S.[daily_nTQty]
								,[daily_nRef] = S.[daily_nRef]
								,[daily_nHigh] = S.[daily_nHigh]
								,[daily_nLow] = S.[daily_nLow]
								,[daily_nOpen] = S.[daily_nOpen]
								,[daily_nClose] = S.[daily_nClose]
								,[daily_percent] = S.[daily_percent]
								,[daily_sStockIdx] = S.[daily_sStockIdx]
								WHEN NOT MATCHED THEN    
								INSERT ([daily_date], [daily_stock_num],[daily_stock_name],[daily_nTQty],[daily_nRef],[daily_nHigh],[daily_nLow],[daily_nOpen],[daily_nClose],[daily_percent],[daily_sStockIdx])
								VALUES (S.[daily_date], S.[daily_stock_num],S.[daily_stock_name],S.[daily_nTQty],S.[daily_nRef],S.[daily_nHigh],S.[daily_nLow],S.[daily_nOpen],S.[daily_nClose],S.[daily_percent],S.[daily_sStockIdx]);";
                        sqlquery = sqlquery.Replace("{#daily_date}", datetime);
                        sqlquery = sqlquery.Replace("{#daily_stock_num}", stock_num);
                        sqlquery = sqlquery.Replace("{#daily_stock_name}", stock_name);
                        sqlquery = sqlquery.Replace("{#daily_nTQty}", "0");
                        sqlquery = sqlquery.Replace("{#daily_nRef}", "0");
                        sqlquery = sqlquery.Replace("{#daily_nHigh}", "0");
                        sqlquery = sqlquery.Replace("{#daily_nLow}", "0");
                        sqlquery = sqlquery.Replace("{#daily_nOpen}", "0");
                        sqlquery = sqlquery.Replace("{#daily_nClose}", "0");
                        sqlquery = sqlquery.Replace("{#daily_percent}", "0");
                        sqlquery = sqlquery.Replace("{#daily_sStockIdx}", "0");
                        SQL.Execute(sqlquery);
                    }
                    else
                    {
                        //更新個股資訊
                        string nTradingDay = pSKStock.nTradingDay.ToString();
                        string date = nTradingDay.Substring(0, 4) + "-" + nTradingDay.Substring(4, 2) + "-" + nTradingDay.Substring(6, 2);
                        stock_name = pSKStock.bstrStockName;
                        int nTQty = pSKStock.nTQty;
                        int nYQty = pSKStock.nYQty;
                        int nStockIdx = pSKStock.nStockIdx;
                        decimal nRef = decimal.Round(Convert.ToDecimal(pSKStock.nRef) / 100, 2);
                        decimal nHigh = decimal.Round(Convert.ToDecimal(pSKStock.nHigh) / 100, 2);
                        decimal nLow = decimal.Round(Convert.ToDecimal(pSKStock.nLow) / 100, 2);
                        decimal nOpen = decimal.Round(Convert.ToDecimal(pSKStock.nOpen) / 100, 2);
                        decimal nClose = decimal.Round(Convert.ToDecimal(pSKStock.nClose) / 100, 2);
                        decimal daily_percent = decimal.Round((nOpen - nRef) / nRef * 100, 3);
                        decimal daily_raise = decimal.Round((nHigh - nLow) / nRef, 3);
                        string sqlquery = @"MERGE [dbo].[daily_data] AS T
								USING (VALUES('{#daily_date}','{#daily_stock_num}','{#daily_stock_name}','{#daily_nTQty}','{#daily_nRef}','{#daily_nHigh}','{#daily_nLow}','{#daily_nOpen}','{#daily_nClose}','{#daily_percent}','{#daily_sStockIdx}','{#daily_raise}'))
								AS S ([daily_date], [daily_stock_num],[daily_stock_name],[daily_nTQty],[daily_nRef],[daily_nHigh],[daily_nLow],[daily_nOpen],[daily_nClose],[daily_percent],[daily_sStockIdx],[daily_raise])
								ON (S.[daily_date] = T.[daily_date] AND S.[daily_stock_num] = T.[daily_stock_num] )
								WHEN MATCHED THEN 
								UPDATE SET [daily_stock_name] = S.[daily_stock_name]
								,[daily_nTQty] = S.[daily_nTQty]
								,[daily_nRef] = S.[daily_nRef]
								,[daily_nHigh] = S.[daily_nHigh]
								,[daily_nLow] = S.[daily_nLow]
								,[daily_nOpen] = S.[daily_nOpen]
								,[daily_nClose] = S.[daily_nClose]
								,[daily_percent] = S.[daily_percent]
								,[daily_sStockIdx] = S.[daily_sStockIdx]
                                ,[daily_raise] = S.[daily_raise]
								WHEN NOT MATCHED THEN    
								INSERT ([daily_date], [daily_stock_num],[daily_stock_name],[daily_nTQty],[daily_nRef],[daily_nHigh],[daily_nLow],[daily_nOpen],[daily_nClose],[daily_percent],[daily_sStockIdx],[daily_raise])
								VALUES (S.[daily_date], S.[daily_stock_num],S.[daily_stock_name],S.[daily_nTQty],S.[daily_nRef],S.[daily_nHigh],S.[daily_nLow],S.[daily_nOpen],S.[daily_nClose],S.[daily_percent],S.[daily_sStockIdx],S.[daily_raise]);";
                        sqlquery = sqlquery.Replace("{#daily_date}", date);
                        sqlquery = sqlquery.Replace("{#daily_stock_num}", stock_num);
                        sqlquery = sqlquery.Replace("{#daily_stock_name}", stock_name);
                        sqlquery = sqlquery.Replace("{#daily_nTQty}", nTQty.ToString());
                        sqlquery = sqlquery.Replace("{#daily_nRef}", nRef.ToString());
                        sqlquery = sqlquery.Replace("{#daily_nHigh}", nHigh.ToString());
                        sqlquery = sqlquery.Replace("{#daily_nLow}", nLow.ToString());
                        sqlquery = sqlquery.Replace("{#daily_nOpen}", nOpen.ToString());
                        sqlquery = sqlquery.Replace("{#daily_nClose}", nClose.ToString());
                        sqlquery = sqlquery.Replace("{#daily_percent}", daily_percent.ToString());
                        sqlquery = sqlquery.Replace("{#daily_sStockIdx}", pSKStock.nStockIdx.ToString());
                        sqlquery = sqlquery.Replace("{#daily_raise}", daily_raise.ToString());
                        SQL.Execute(sqlquery);
                        //紀錄基本資訊
                        STOCKS[nStockIdx].Stock_Num = stock_num;
                        STOCKS[nStockIdx].Stock_Name = row["Stock_Name"].ToString();
                        STOCKS[nStockIdx].onHand = 0;
                        STOCKS[nStockIdx].nRef = nRef;
                        STOCKS[nStockIdx].nTQty = nTQty;
                        STOCKS[nStockIdx].nOpen = nOpen;
                        STOCKS[nStockIdx].nHigh = nHigh;
                        STOCKS[nStockIdx].nHigh_Time = DateTime.Now;
                        STOCKS[nStockIdx].nLow = nLow;
                        STOCKS[nStockIdx].nLow_Time = DateTime.Now;
                        STOCKS[nStockIdx].nHigh = nHigh;
                        STOCKS[nStockIdx].nYHigh = Convert.ToDecimal(row["daily_nHigh"].ToString() == "" ? "0" : row["daily_nHigh"].ToString());
                        STOCKS[nStockIdx].nYLow = Convert.ToDecimal(row["daily_nLow"].ToString() == "" ? "0" : row["daily_nLow"].ToString());
                        STOCKS[nStockIdx].nYQty = Convert.ToInt32(row["daily_nTQty"].ToString() == "" ? "0" : row["daily_nTQty"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                SQL.Add_SQL_Log("Update_Stock_Info err", ex.Message);

            }
        }

        /*** 群益事件 Start ***/

        /// <summary>
        /// 群益主機回傳公告內容
        /// </summary>
        void OnAnnouncement(string strUserID, string bstrMessage, out short nConfirmCode)
        {
            //WriteMessage(strUserID + "_" + bstrMessage);
            nConfirmCode = -1;
        }

        /// <summary>
        /// 國內報價連線回應事件
        /// </summary>
        /// <param name="nKind"></param>
        /// <param name="nCode"></param>
        void m_SKQuoteLib_OnConnection(int nKind, int nCode)
        {
            try
            {
                if (nKind == 3001)
                {
                    if (nCode == 0)
                    {
                        WriteMessage("國內報價連線中");

                    }
                }
                else if (nKind == 3002)
                {
                    WriteMessage("國內報價連線中斷");

                }
                else if (nKind == 3003)
                {
                    WriteMessage("國內報價連線成功");
                    //宣告定時任務去取得伺服器時間以免斷線
                    System.Timers.Timer Call_SKCOM_Timer = new System.Timers.Timer();
                    Call_SKCOM_Timer.Enabled = true;
                    Call_SKCOM_Timer.Interval = 15000;//間隔15秒 
                    Call_SKCOM_Timer.Start();
                    Call_SKCOM_Timer.Elapsed += new System.Timers.ElapsedEventHandler(Call_SKCOM_Regular);
                    SKQuoteLib_RequestStocks();
                    Update_Stock_Info();
                    SKCOM_RequestTicks_and_Best5();

                }
                else if (nKind == 3021)
                {
                    WriteMessage("網路斷線");

                }
            }
            catch (Exception ex)
            {
                WriteMessage("連線出現非預期錯誤，錯誤訊息為：" + ex.Message);
            }

        }

        /// <summary>
        /// 群益報價主機回傳時間事件
        /// </summary>
        void m_SKQuoteLib_OnNotifyServerTime(short sHour, short sMinute, short sSecond, int nTotal)
        {
            Server_Time.Text = sHour.ToString("D2") + ":" + sMinute.ToString("D2") + ":" + sSecond.ToString("D2");
            try
            {
                AsyncTaskDeal("SKCOM_RequestTicks");
            }
            catch (Exception ex)
            {
                string connent = "Add_Err_Msg" + "@" + "SKCOM_RequestTicks_Err" + "@" + ex.Message;
                AsyncTaskDeal(connent);
                throw;
            }
        }

        /// <summary>
        /// 帳號資訊。透過呼叫GetUserAccount後，帳號資訊由該事件回傳。
        /// </summary>
        /// <param name="bstrLogInID">登入ID</param>
        /// <param name="bstrAccountData">帳號資訊。以逗點分隔每一個欄位，欄位依序為：『市場,分公司,分公司代號,帳號,身份證字號,姓名』</param>
        void m_OrderObj_OnAccount(string bstrLogInID, string bstrAccountData)
        {
            //strValues[0]:市場
            //strValues[1]:分公司
            //strValues[2]:分公司代號
            //strValues[3]:帳號
            //strValues[4]:身份證字號
            //strValues[5]:姓名			
            string[] strValues;
            string strAccount;

            strValues = bstrAccountData.Split(',');
            strAccount = bstrLogInID + " " + strValues[1] + strValues[3];
            if (strValues[0] == "TS")
            {
                API_Order_Account.Items.Add("證券 " + strAccount);
                FULL_ACCOUNT = strValues[1] + strValues[3];
                WriteMessage("已取得下單帳號：" + FULL_ACCOUNT);
                try
                {
                    API_Order_Account.SelectedIndex = 0;
                }
                catch (Exception ex)
                {
                    TOOL.Add_Log("m_OrderObj_OnAccount-error", ex.Message);
                }

            }

        }

        /// <summary>
        /// 當有回報將主動呼叫函式，並通知委託的狀態。
        /// </summary>
        /// <param name="strUserID">登入ID</param>
        /// <param name="strData">每一筆資料以「,」分隔每一個欄位</param>
        void OnData(string strUserID, string strData)
        {
            string[] strValues;
            strValues = strData.Split(',');
            WriteMessage(strData);
        }

        /// <summary>
        /// 證券即時庫存。透過呼叫GetRealBalanceReport後，資訊由該事件回傳。
        /// </summary>
        /// <param name="bstrData">證券庫存資料</param>
        void m_pSKOrder_OnRealBalanceReport(string bstrData)
        {
            WriteMessage(bstrData);
        }

        /// <summary>
        /// 群益報價主機回傳TICK資料
        /// </summary>
        /// <param name="sMarketNo">報價有異動的商品市場別</param>
        /// <param name="sStockIdx">系統所編的索引代碼</param>
        /// <param name="nPtr">第幾筆成交明細, 由0開始</param>
        /// <param name="nDate">交易日(YYYYMMDD)</param>
        /// <param name="lTimehms">時間(時分秒hh:mm:ss)</param>
        /// <param name="lTimemillismicros">時間(毫秒微秒"ms"μs)</param>
        /// <param name="nBid">買價</param>
        /// <param name="nAsk">賣價</param>
        /// <param name="nClose">成交價</param>
        /// <param name="nQty">量</param>
        /// <param name="nSimulate">揭示 0:一般 1:試算</param>
        void m_SKQuoteLib_OnNotifyTicks(short sMarketNo, int nStockIdx, int nPtr, int nDate, int nTimehms, int nTimemillismicros, int nBid, int nAsk, int nClose, int nQty, int nSimulate)
        {

            Process_Tick_Data(sMarketNo, nStockIdx, nPtr, nDate, nTimehms, nTimemillismicros, nBid, nAsk, nClose, nQty, nSimulate);

        }

        /// <summary>
        /// 群益報價主機回傳歷史TICK資料
        /// </summary>
        /// <param name="sMarketNo">報價有異動的商品市場別</param>
        /// <param name="sStockIdx">系統所編的索引代碼</param>
        /// <param name="nPtr">第幾筆成交明細, 由0開始</param>
        /// <param name="nDate">交易日(YYYYMMDD)</param>
        /// <param name="lTimehms">時間(時分秒hh:mm:ss)</param>
        /// <param name="lTimemillismicros">時間(毫秒微秒"ms"μs)</param>
        /// <param name="nBid">買價</param>
        /// <param name="nAsk">賣價</param>
        /// <param name="nClose">成交價</param>
        /// <param name="nQty">量</param>
        /// <param name="nSimulate">揭示 0:一般 1:試算</param>
        void m_SKQuoteLib_OnNotifyHistoryTicks(short sMarketNo, int nStockIdx, int nPtr, int nDate, int nTimehms, int nTimemillismicros, int nBid, int nAsk, int nClose, int nQty, int nSimulate)
        {
            Process_Tick_Data(sMarketNo, nStockIdx, nPtr, nDate, nTimehms, nTimemillismicros, nBid, nAsk, nClose, nQty, nSimulate);
        }

        public void Process_Tick_Data(short sMarketNo, int nStockIdx, int nPtr, int nDate, int nTimehms, int nTimemillismicros, int nBid, int nAsk, int nClose, int nQty, int nSimulate)
        {
            if (nSimulate == 1) return;
            try
            {
                if (!CHECK_TICK_TABLE)
                {
                    string query = @"CREATE TABLE [dbo].[tick_" + nDate.ToString() + @"](
	                                [tick_time] [datetime2](7) NULL,
	                                [sMarketNo] [varchar](50) NULL,
	                                [stock_num] [varchar](50) NULL,
	                                [nStockIdx] [varchar](50) NOT NULL,
	                                [nPtr] [varchar](50) NOT NULL,
	                                [nDate] [varchar](50) NULL,
	                                [nTimehms] [varchar](50) NULL,
	                                [nTimemillismicros] [varchar](50) NULL,
	                                [nBid] [decimal](12, 3) NULL,
	                                [nAsk] [decimal](12, 3) NULL,
	                                [nClose] [decimal](12, 3) NULL,
	                                [nQty] [int] NULL,
	                                [nSimulate] [int] NULL,
                                 CONSTRAINT [PK_tick_" + nDate.ToString() + @"] PRIMARY KEY CLUSTERED 
                                (
	                                [nStockIdx] ASC,
	                                [nPtr] ASC
                                )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
                                ) ON [PRIMARY]";
                    SQL.Execute(query);
                    CHECK_TICK_TABLE = true;
                }
                //decimal d_nBid = TOOL.Convert_Tick_Price_toDecimal(nBid);
                //decimal d_nAsk = TOOL.Convert_Tick_Price_toDecimal(nAsk);
                //decimal d_nClose = TOOL.Convert_Tick_Price_toDecimal(nClose);
                string tick_time = TOOL.Tick_Convert_To_Datetime(nDate, nTimehms, nTimemillismicros);
                //TickObject temp_tick = new TickObject();
                //temp_tick.sMarketNo = sMarketNo;
                //temp_tick.nStockIdx = nStockIdx;
                //temp_tick.nPtr = nPtr;
                //temp_tick.nDate = nDate;
                //temp_tick.nTimehms = nTimehms;
                //temp_tick.nTimemillismicros = nTimemillismicros;
                //temp_tick.nBid = d_nBid;
                //temp_tick.nAsk = d_nAsk;
                //temp_tick.nClose = d_nClose;
                //temp_tick.nQty = nQty;
                //temp_tick.tick_time = tick_time;
                //temp_tick.receive_time = DateTime.Now;
                //temp_tick.SQLOb = SQL;
                //temp_tick.ToolOb = TOOL;
                //temp_tick.StockOb = STOCKS[nStockIdx];
                //switch (TICKCOUNT % 5)
                //{
                //    case 0:
                //        Thread Store_Tick_t0 = new Thread(temp_tick.Store_Tick);
                //        Store_Tick_t0.Start();
                //        break;
                //    case 1:
                //        Thread Store_Tick_t1 = new Thread(temp_tick.Store_Tick);
                //        Store_Tick_t1.Start();
                //        break;
                //    case 2:
                //        Thread Store_Tick_t2 = new Thread(temp_tick.Store_Tick);
                //        Store_Tick_t2.Start();
                //        break;
                //    case 3:
                //        Thread Store_Tick_t3 = new Thread(temp_tick.Store_Tick);
                //        Store_Tick_t3.Start();
                //        break;
                //    case 4:
                //        Thread Store_Tick_t4 = new Thread(temp_tick.Store_Tick);
                //        Store_Tick_t4.Start();
                //        break;
                //    default:
                //        break;
                //}

                string sqlquery = @"MERGE [dbo].[tick_" + nDate + @"] AS T
								USING (VALUES('" + nStockIdx + @"','" + nPtr + @"','" + nSimulate + @"','" + tick_time + @"','" + sMarketNo + @"','" + nDate + @"','" + nTimehms + @"','" + nTimemillismicros + @"','" + nBid + @"','" + nAsk + @"','" + nClose + @"','" + nQty + @"','" + STOCKS[nStockIdx].Stock_Num + @"'))
								AS S ([nStockIdx], [nPtr],[nSimulate],[tick_time],[sMarketNo],[nDate],[nTimehms],[nTimemillismicros],[nBid],[nAsk],[nClose],[nQty],[stock_num])
								ON (S.[nStockIdx] = T.[nStockIdx] AND S.[nPtr] = T.[nPtr] )
								WHEN MATCHED THEN 
								UPDATE SET [nSimulate] = S.[nSimulate]
								,[tick_time] = S.[tick_time]
								,[sMarketNo] = S.[sMarketNo]
								,[nDate] = S.[nDate]
								,[nTimehms] = S.[nTimehms]
								,[nTimemillismicros] = S.[nTimemillismicros]
								,[nBid] = S.[nBid]
                                ,[nAsk] = S.[nAsk]
								,[nClose] = S.[nClose]
								,[nQty] = S.[nQty]
                                ,[stock_num] = S.[stock_num]
								WHEN NOT MATCHED THEN    
								INSERT ([nStockIdx], [nPtr],[nSimulate],[tick_time],[sMarketNo],[nDate],[nTimehms],[nTimemillismicros],[nBid],[nAsk],[nClose],[nQty],[stock_num])
								VALUES (S.[nStockIdx], S.[nPtr],S.[nSimulate],S.[tick_time],S.[sMarketNo],S.[nDate],S.[nTimehms],S.[nTimemillismicros],S.[nBid],S.[nAsk],S.[nClose],S.[nQty],S.[stock_num]);";
                SQL.Execute(sqlquery);

            }
            catch (Exception ex)
            {
                SQL.Add_SQL_Log("tick錯誤", ex.Message);
            }
            TICKCOUNT++;
            Tick_Counter.Text = TICKCOUNT.ToString();
            Application.DoEvents();
        }

        /*** 群益事件 End ***/
        private void Easy_Login_Btn_Click(object sender, EventArgs e)
        {
            Excute_Trade_Task();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string selectedDate = DateTime.Parse(SelectDate.Text).ToString("yyyy-MM-dd");
            Download_Tick_Data(selectedDate);
            //Download_Daily_Data(selectedDate);
            //Download_Best5_Data(selectedDate);
            //Delete_Tick_Table();
        }

        private void Tick_Counter_TextChanged(object sender, EventArgs e)
        {
            LAST_TICK_RECEIVE = DateTime.Now;
            Application.DoEvents();
        }

        private void Open_Log_Folder_btn_Click(object sender, EventArgs e)
        {
            if (Log_Path.Text.ToString() != "")
            {
                TOOL.Open_File_Folder(Log_Path.Text + @"\output\Relesae\");
            }
            else
            {
                MessageBox.Show("尚未指定Log資料夾");
            }
        }
    }
}
