﻿
namespace SKCOM_Record_Tool
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.Request_Max_Num = new System.Windows.Forms.TextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.SQL_Connecting_Btn = new System.Windows.Forms.Button();
            this.SQL_Connecting_State = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.SQL_PWD = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.SQL_UID = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.SQL_DB = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.SQL_Server = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.Open_Log_Folder_btn = new System.Windows.Forms.Button();
            this.Log_Path = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.API_Order_Account = new System.Windows.Forms.ComboBox();
            this.API_Password = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.API_Account = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.listInformation = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Tick_Counter = new System.Windows.Forms.Label();
            this.Easy_Login_Btn = new System.Windows.Forms.Button();
            this.Server_Time = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Reply_State = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Quote_State = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Server_State = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SelectDate = new System.Windows.Forms.DateTimePicker();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 207);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1102, 343);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.SelectDate);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.Request_Max_Num);
            this.tabPage1.Controls.Add(this.label68);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1094, 317);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "設定";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(705, 238);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 19;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Request_Max_Num
            // 
            this.Request_Max_Num.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.Request_Max_Num.Location = new System.Drawing.Point(139, 240);
            this.Request_Max_Num.Name = "Request_Max_Num";
            this.Request_Max_Num.Size = new System.Drawing.Size(54, 22);
            this.Request_Max_Num.TabIndex = 18;
            this.Request_Max_Num.Text = "2000";
            this.Request_Max_Num.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(14, 245);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(125, 12);
            this.label68.TabIndex = 17;
            this.label68.Text = "●訂閱股票的上限數量";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.SQL_Connecting_Btn);
            this.groupBox3.Controls.Add(this.SQL_Connecting_State);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.SQL_PWD);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.SQL_UID);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.SQL_DB);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.SQL_Server);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Location = new System.Drawing.Point(6, 113);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1082, 117);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "資料庫";
            // 
            // SQL_Connecting_Btn
            // 
            this.SQL_Connecting_Btn.Font = new System.Drawing.Font("新細明體", 16F);
            this.SQL_Connecting_Btn.Location = new System.Drawing.Point(865, 67);
            this.SQL_Connecting_Btn.Name = "SQL_Connecting_Btn";
            this.SQL_Connecting_Btn.Size = new System.Drawing.Size(198, 33);
            this.SQL_Connecting_Btn.TabIndex = 10;
            this.SQL_Connecting_Btn.Text = "測試連線";
            this.SQL_Connecting_Btn.UseVisualStyleBackColor = true;
            // 
            // SQL_Connecting_State
            // 
            this.SQL_Connecting_State.AutoSize = true;
            this.SQL_Connecting_State.Font = new System.Drawing.Font("新細明體", 16F);
            this.SQL_Connecting_State.Location = new System.Drawing.Point(987, 25);
            this.SQL_Connecting_State.Name = "SQL_Connecting_State";
            this.SQL_Connecting_State.Size = new System.Drawing.Size(76, 22);
            this.SQL_Connecting_State.TabIndex = 9;
            this.SQL_Connecting_State.Text = "未連線";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("新細明體", 16F);
            this.label13.Location = new System.Drawing.Point(861, 25);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(120, 22);
            this.label13.TabIndex = 8;
            this.label13.Text = "連線狀態：";
            // 
            // SQL_PWD
            // 
            this.SQL_PWD.Font = new System.Drawing.Font("新細明體", 16F);
            this.SQL_PWD.Location = new System.Drawing.Point(401, 73);
            this.SQL_PWD.Name = "SQL_PWD";
            this.SQL_PWD.PasswordChar = '*';
            this.SQL_PWD.Size = new System.Drawing.Size(173, 33);
            this.SQL_PWD.TabIndex = 7;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("新細明體", 16F);
            this.label11.Location = new System.Drawing.Point(310, 78);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(78, 22);
            this.label11.TabIndex = 6;
            this.label11.Text = "PWD：";
            // 
            // SQL_UID
            // 
            this.SQL_UID.Font = new System.Drawing.Font("新細明體", 16F);
            this.SQL_UID.Location = new System.Drawing.Point(401, 25);
            this.SQL_UID.Name = "SQL_UID";
            this.SQL_UID.Size = new System.Drawing.Size(173, 33);
            this.SQL_UID.TabIndex = 5;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("新細明體", 16F);
            this.label12.Location = new System.Drawing.Point(310, 30);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(69, 22);
            this.label12.TabIndex = 4;
            this.label12.Text = "UID：";
            // 
            // SQL_DB
            // 
            this.SQL_DB.Font = new System.Drawing.Font("新細明體", 16F);
            this.SQL_DB.Location = new System.Drawing.Point(97, 73);
            this.SQL_DB.Name = "SQL_DB";
            this.SQL_DB.Size = new System.Drawing.Size(173, 33);
            this.SQL_DB.TabIndex = 3;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("新細明體", 16F);
            this.label10.Location = new System.Drawing.Point(6, 78);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(61, 22);
            this.label10.TabIndex = 2;
            this.label10.Text = "DB：";
            // 
            // SQL_Server
            // 
            this.SQL_Server.Font = new System.Drawing.Font("新細明體", 16F);
            this.SQL_Server.Location = new System.Drawing.Point(97, 25);
            this.SQL_Server.Name = "SQL_Server";
            this.SQL_Server.Size = new System.Drawing.Size(173, 33);
            this.SQL_Server.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("新細明體", 16F);
            this.label9.Location = new System.Drawing.Point(6, 30);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(85, 22);
            this.label9.TabIndex = 0;
            this.label9.Text = "Server：";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.Open_Log_Folder_btn);
            this.groupBox2.Controls.Add(this.Log_Path);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.API_Order_Account);
            this.groupBox2.Controls.Add(this.API_Password);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.API_Account);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Location = new System.Drawing.Point(6, 20);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1082, 76);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "群益API";
            // 
            // Open_Log_Folder_btn
            // 
            this.Open_Log_Folder_btn.Location = new System.Drawing.Point(988, 20);
            this.Open_Log_Folder_btn.Name = "Open_Log_Folder_btn";
            this.Open_Log_Folder_btn.Size = new System.Drawing.Size(75, 23);
            this.Open_Log_Folder_btn.TabIndex = 10;
            this.Open_Log_Folder_btn.Text = "開啟log";
            this.Open_Log_Folder_btn.UseVisualStyleBackColor = true;
            this.Open_Log_Folder_btn.Click += new System.EventHandler(this.Open_Log_Folder_btn_Click);
            // 
            // Log_Path
            // 
            this.Log_Path.Location = new System.Drawing.Point(519, 53);
            this.Log_Path.Name = "Log_Path";
            this.Log_Path.Size = new System.Drawing.Size(343, 22);
            this.Log_Path.TabIndex = 8;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("新細明體", 9F);
            this.label17.Location = new System.Drawing.Point(448, 58);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(60, 12);
            this.label17.TabIndex = 7;
            this.label17.Text = "Log路徑：";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("新細明體", 9F);
            this.label16.Location = new System.Drawing.Point(8, 58);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(65, 12);
            this.label16.TabIndex = 6;
            this.label16.Text = "下單帳號：";
            // 
            // API_Order_Account
            // 
            this.API_Order_Account.FormattingEnabled = true;
            this.API_Order_Account.Location = new System.Drawing.Point(79, 54);
            this.API_Order_Account.Name = "API_Order_Account";
            this.API_Order_Account.Size = new System.Drawing.Size(356, 20);
            this.API_Order_Account.TabIndex = 5;
            // 
            // API_Password
            // 
            this.API_Password.Location = new System.Drawing.Point(384, 21);
            this.API_Password.Name = "API_Password";
            this.API_Password.PasswordChar = '*';
            this.API_Password.Size = new System.Drawing.Size(259, 22);
            this.API_Password.TabIndex = 4;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("新細明體", 9F);
            this.label15.Location = new System.Drawing.Point(337, 26);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 12);
            this.label15.TabIndex = 3;
            this.label15.Text = "密碼：";
            // 
            // API_Account
            // 
            this.API_Account.Location = new System.Drawing.Point(55, 21);
            this.API_Account.Name = "API_Account";
            this.API_Account.Size = new System.Drawing.Size(259, 22);
            this.API_Account.TabIndex = 2;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("新細明體", 9F);
            this.label14.Location = new System.Drawing.Point(8, 26);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 12);
            this.label14.TabIndex = 1;
            this.label14.Text = "帳號：";
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1094, 317);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "資料下載";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // listInformation
            // 
            this.listInformation.FormattingEnabled = true;
            this.listInformation.ItemHeight = 12;
            this.listInformation.Location = new System.Drawing.Point(12, 77);
            this.listInformation.Name = "listInformation";
            this.listInformation.Size = new System.Drawing.Size(1102, 124);
            this.listInformation.TabIndex = 4;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Tick_Counter);
            this.groupBox1.Controls.Add(this.Easy_Login_Btn);
            this.groupBox1.Controls.Add(this.Server_Time);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.Reply_State);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.Quote_State);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.Server_State);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1102, 52);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "狀態列";
            // 
            // Tick_Counter
            // 
            this.Tick_Counter.AutoSize = true;
            this.Tick_Counter.Location = new System.Drawing.Point(729, 27);
            this.Tick_Counter.Name = "Tick_Counter";
            this.Tick_Counter.Size = new System.Drawing.Size(11, 12);
            this.Tick_Counter.TabIndex = 10;
            this.Tick_Counter.Text = "0";
            this.Tick_Counter.TextChanged += new System.EventHandler(this.Tick_Counter_TextChanged);
            // 
            // Easy_Login_Btn
            // 
            this.Easy_Login_Btn.Location = new System.Drawing.Point(806, 20);
            this.Easy_Login_Btn.Name = "Easy_Login_Btn";
            this.Easy_Login_Btn.Size = new System.Drawing.Size(75, 23);
            this.Easy_Login_Btn.TabIndex = 9;
            this.Easy_Login_Btn.Text = "一鍵登入";
            this.Easy_Login_Btn.UseVisualStyleBackColor = true;
            this.Easy_Login_Btn.Click += new System.EventHandler(this.Easy_Login_Btn_Click);
            // 
            // Server_Time
            // 
            this.Server_Time.AutoSize = true;
            this.Server_Time.Font = new System.Drawing.Font("新細明體", 16F);
            this.Server_Time.Location = new System.Drawing.Point(1001, 18);
            this.Server_Time.Name = "Server_Time";
            this.Server_Time.Size = new System.Drawing.Size(82, 22);
            this.Server_Time.TabIndex = 7;
            this.Server_Time.Text = "00:00:00";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("新細明體", 16F);
            this.label7.Location = new System.Drawing.Point(887, 18);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(120, 22);
            this.label7.TabIndex = 6;
            this.label7.Text = "主機時間：";
            // 
            // Reply_State
            // 
            this.Reply_State.AutoSize = true;
            this.Reply_State.Font = new System.Drawing.Font("新細明體", 16F);
            this.Reply_State.Location = new System.Drawing.Point(583, 18);
            this.Reply_State.Name = "Reply_State";
            this.Reply_State.Size = new System.Drawing.Size(76, 22);
            this.Reply_State.TabIndex = 5;
            this.Reply_State.Text = "未連線";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("新細明體", 16F);
            this.label6.Location = new System.Drawing.Point(457, 18);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(120, 22);
            this.label6.TabIndex = 4;
            this.label6.Text = "委託回報：";
            // 
            // Quote_State
            // 
            this.Quote_State.AutoSize = true;
            this.Quote_State.Font = new System.Drawing.Font("新細明體", 16F);
            this.Quote_State.Location = new System.Drawing.Point(357, 18);
            this.Quote_State.Name = "Quote_State";
            this.Quote_State.Size = new System.Drawing.Size(76, 22);
            this.Quote_State.TabIndex = 3;
            this.Quote_State.Text = "未連線";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("新細明體", 16F);
            this.label4.Location = new System.Drawing.Point(231, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 22);
            this.label4.TabIndex = 2;
            this.label4.Text = "報價主機：";
            // 
            // Server_State
            // 
            this.Server_State.AutoSize = true;
            this.Server_State.Font = new System.Drawing.Font("新細明體", 16F);
            this.Server_State.Location = new System.Drawing.Point(132, 18);
            this.Server_State.Name = "Server_State";
            this.Server_State.Size = new System.Drawing.Size(76, 22);
            this.Server_State.TabIndex = 1;
            this.Server_State.Text = "未連線";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("新細明體", 16F);
            this.label1.Location = new System.Drawing.Point(6, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "群益主機：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 276);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 12);
            this.label2.TabIndex = 20;
            this.label2.Text = "●指定資料日期";
            // 
            // SelectDate
            // 
            this.SelectDate.Location = new System.Drawing.Point(103, 269);
            this.SelectDate.Name = "SelectDate";
            this.SelectDate.Size = new System.Drawing.Size(200, 22);
            this.SelectDate.TabIndex = 21;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1120, 562);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.listInformation);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button SQL_Connecting_Btn;
        private System.Windows.Forms.Label SQL_Connecting_State;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox SQL_PWD;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox SQL_UID;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox SQL_DB;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox SQL_Server;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button Open_Log_Folder_btn;
        private System.Windows.Forms.TextBox Log_Path;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox API_Order_Account;
        private System.Windows.Forms.TextBox API_Password;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox API_Account;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ListBox listInformation;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label Tick_Counter;
        private System.Windows.Forms.Button Easy_Login_Btn;
        private System.Windows.Forms.Label Server_Time;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label Reply_State;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label Quote_State;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label Server_State;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Request_Max_Num;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DateTimePicker SelectDate;
        private System.Windows.Forms.Label label2;
    }
}

